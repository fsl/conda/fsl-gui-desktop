#!/usr/bin/env bash

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

export GUI_NAME="fsl-desktop"

# use uname to get the system type
if [[ $(uname) == "Darwin" ]]; then
    export SYSTEM_TYPE="Mac"
    export SYSTEM_TYPE_OUT="darwin"
else
    export SYSTEM_TYPE="Linux"
    export SYSTEM_TYPE_OUT="linux"
fi

# use uname to get the arch
if [[ $(uname -m) == "x86_64" ]]; then
    export ARCH="X64"
    export ARCH_OUT="x64"
else
    export ARCH="Arm64"
    export ARCH_OUT="arm64"
fi

# concatenate the system type and arch to get the platform
export PLATFORM=$SYSTEM_TYPE$ARCH

# install gui deps
npm install
# build the application
npm run "make$PLATFORM"
# the npm run build command will create an out folder with the electron app
mkdir -p $PREFIX/share/fsl/gui/desktop
# unzip the electron app into the proper location
unzip -qq -o ./out/make/zip/$SYSTEM_TYPE_OUT/$ARCH_OUT/*.zip -d $PREFIX/share/fsl/gui/desktop
# copy the app zip to the destination folder
# cp -r ./out/make/zip/$SYSTEM_TYPE_OUT/$ARCH_OUT/* $PREFIX/share/fsl/gui/desktop/


# Install an entry point for the desktop launcher into $FSLDIR/bin/
if [[ "${OSTYPE}" == "darwin"* ]]; then
    targetdir="${PREFIX}/share/fsl/gui/desktop/fsl-desktop.app/Contents/MacOS/"
else
    targetdir="${PREFIX}/share/fsl/gui/desktop/fsl-desktop-linux-x64/"
fi

${FSLDIR}/share/fsl/sbin/createFSLWrapper \
   -s ${targetdir} -d ${FSLDIR}/bin       \
   -f -r fsl-desktop=fsl_gui_launcher
